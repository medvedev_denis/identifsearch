import java.io.*;
import java.util.regex.*;


/**
 * Класс для нахождения идентификаторов в java классе.
 *
 * @author Denis
 */
// На 80% схоже с примером.
public class IdentifSearch {
    public static void main(String[] args) throws IOException {
        Pattern searchPattern = Pattern.compile("[A-Za-z_](\\w?)+");
        Matcher matcher = searchPattern.matcher("");
        try (BufferedReader reader = new BufferedReader(new FileReader("Input.java"));
             BufferedWriter writer = new BufferedWriter(new FileWriter("Output.txt"));) {
            String s;
            while ((s = reader.readLine()) != null) {
                matcher.reset(s);
                while (matcher.find()) {
                    writer.write(matcher.group() + "\n");
                }
            }
            System.out.println("Процесс выполнен успешно");
        } catch (IOException e){
            System.out.println("Файл не найден");
        }
    }
}

